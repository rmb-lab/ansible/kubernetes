---
- name: reset kubernetes nodes
  hosts: all
  remote_user: cloud-user
  become: yes
  become_user: root
  tasks:
    - name: RUN KUBEADM RESET
      shell: kubeadm reset --force --cri-socket=/var/run/containerd/containerd.sock

    - name: STOP CONTAINERD
      systemd:
        name: containerd
        state: stopped

    - name: UNMOUNT CONTAINERD VOLUME
      mount:
        path: /data/var/lib/containerd
        src: /dev/mapper/data-lv_data_containerd
        fstype: xfs
        dump: 1
        passno: 2
        state: absent

    - name: UNMOUNT KUBELET VOLUME
      mount:
        path: /data/var/lib/kubelet
        src: /dev/mapper/data-lv_data_kubelet
        fstype: xfs
        dump: 1
        passno: 2
        state: absent

    - name: UNMOUNT ETCD VOLUME
      mount:
        path: /data/var/lib/etcd
        src: /dev/mapper/data-lv_data_etcd
        fstype: xfs
        dump: 1
        passno: 2
        state: absent
      when: "inventory_hostname in groups['masters']"

    - name: REMOVE CONTAINERD LOGICAL VOLUME
      lvol:
        vg: data
        lv: lv_data_containerd
        force: yes
        state: absent

    - name: REMOVE KUBELET LOGICAL VOLUME
      lvol:
        vg: data
        lv: lv_data_kubelet
        force: yes
        state: absent

    - name: REMOVE ETCD LOGICAL VOLUME
      lvol:
        vg: data
        lv: lv_data_etcd
        force: yes
        state: absent

    - name: REMOVE MASTER KUBECONFIG
      file:
        path: /root/.kube/config
        state: absent
      when: "inventory_hostname in groups['masters']"

    - name: REMOVE CNI CONFIG
      file:
        path: /etc/cni/net.d/10-kuberouter.conflist
        state: absent

    - name: REMOVE CLUSTER-INFO
      file:
        path: /etc/kubernetes/cluster-info.yaml 
        state: absent

    - name: REMOVE KUBE-BRIDGE, KUBE-DUMMY-IF, AND DUMMY0
      command: "ip link delete {{ item }}"
      ignore_errors: yes
      with_items:
        - kube-bridge
        - kube-dummy-if
        - dummy0

    - name: RESET IPTABLES
      # Hoepfully we aren't actually using iptables for anything because this clears all rules
      shell: iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X

    - name: RESET IPVS
      command: ipvsadm -C

    - name: RESET IPSET
      command: ipset destroy
